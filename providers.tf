terraform {
  required_providers {
    panos = {
      source  = "paloaltonetworks/panos"
    }
  }
}

provider "panos" {
    hostname = "192.168.1.59"
    json_config_file = "./panos-creds.json"
}

