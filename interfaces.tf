# assign interface
resource "panos_ethernet_interface" "ethernet-1-6" {
    name = "ethernet1/6"
    mode = "layer3"
    vsys = "vsys1"
}

resource "panos_ethernet_interface" "ethernet-1-2" {
    name = "ethernet1/2"
    mode = "layer2"
    vsys = "vsys1"
}
