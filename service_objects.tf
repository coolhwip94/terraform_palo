# create service object
resource "panos_service_object" "grafana-service" {
  name             = "grafana-service-3000"
  protocol         = "tcp"
  description      = "Grafana Service"
  destination_port = "3000"
  tags = [
    panos_administrative_tag.internal.name,
  ]
}

resource "panos_service_object" "prometheus-service" {
  name             = "prometheus-service-9090"
  protocol         = "tcp"
  description      = "Prometheus Service"
  destination_port = "9090"
  tags = [
    panos_administrative_tag.internal.name,
  ]
}
