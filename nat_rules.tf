# create nat rule group

resource "panos_nat_rule_group" "main_rule_group" {

    position_keyword = "bottom"

    rule {
        name = "grafana-rule"
        original_packet {
            source_zones = ["untrusted"]
            destination_zone = "untrusted"
            destination_interface = panos_ethernet_interface.ethernet-1-6.name
            source_addresses = [local.prometheus_ip_resource.name]
            destination_addresses = [local.grafana_ip_resource.name]
        }
        translated_packet {
            source {}
            destination {
                static_translation {
                    address = local.grafana_ip_resource.name
                    port = 3000
                }
            }
        }
    }

 rule {
        name = "prometheus-rule"
        original_packet {
            source_zones = ["untrusted"]
            destination_zone = "untrusted"
            destination_interface = panos_ethernet_interface.ethernet-1-6.name
            source_addresses = [local.grafana_ip_resource.name]
            destination_addresses = [local.prometheus_ip_resource.name]
        }
        translated_packet {
            source {}
            destination {
                static_translation {
                    address = local.prometheus_ip_resource.name
                    port = 9090
                }
            }
        }
    }

 rule {
        name = "hwiplab-rule"
        original_packet {
            source_zones = ["untrusted"]
            destination_zone = "untrusted"
            destination_interface = panos_ethernet_interface.ethernet-1-6.name
            source_addresses = [local.grafana_ip_resource.name, local.prometheus_ip_resource.name]
            destination_addresses = [local.hwiplab_ip_resource.name]
        }
        translated_packet {
            source {}
            destination {
                static_translation {
                    address = local.prometheus_ip_resource.name
                    port = 9090
                }
            }
        }
    }
}


