# create untrusted zone
resource "panos_zone" "untrusted" {
    name = "untrusted"
    mode = "layer3"
    interfaces = [
        panos_ethernet_interface.ethernet-1-6.name,
    ]
    enable_user_id = true
}


# create trusted zone
resource "panos_zone" "trusted" {
    name = "trusted"
    mode = "layer2"
    interfaces = [
        panos_ethernet_interface.ethernet-1-2.name,
    ]
    enable_user_id = true
}
