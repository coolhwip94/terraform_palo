# create address object
resource "panos_address_object" "grafana" {
  name        = "grafana"
  value       = local.grafana_ip
  description = "Grafana"
  tags = [
    panos_administrative_tag.internal.name,
  ]
}

resource "panos_address_object" "prometheus" {
  name        = "prometheus"
  value       = local.prometheus_ip
  description = "Prometheus"
  tags = [
    panos_administrative_tag.internal.name,
  ]
}

resource "panos_address_object" "hwiplab" {
  name        = "hwiplab"
  value       = local.hwiplab_ip
  description = "hwiplab"
  tags = [
    panos_administrative_tag.internal.name,
  ]
}

