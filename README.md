# Terraform Palo
> This repo hosts terraform config files for creating various rules within Panos from Palo Alto

## Configuration
---
- Terraform is required.
- Initialize Terraform modules and provider
  ```
  terraform init
  ```
- Create a `panos-creds.json` file and populate with the following credentials.
    > https://registry.terraform.io/providers/PaloAltoNetworks/panos/latest/docs#example-provider-usage (alternate authentication methods)
    ```
    {
        "username": "admin_username",
        "password": "admin_password"
    }
    ```

## Applying Configs
---
- Create a terraform plan
  ```
  terraform plan -out <plan_name.tfplan>

  # example
  terraform plan -out test.tfplan
  ```

- Apply the terraform config
  ```
  terraform apply <plan_name.tfplan>

  # example
  terraform apply test.tfplan
  ```


## References
---
- https://registry.terraform.io/providers/PaloAltoNetworks/panos/latest/docs
- https://registry.terraform.io/providers/PaloAltoNetworks/panos/latest/docs#example-provider-usage