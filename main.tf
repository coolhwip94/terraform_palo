locals {
    grafana_ip = "192.168.1.157/32"
    prometheus_ip = "192.168.1.157/32"
    hwiplab_ip = "192.168.1.136/32"
    grafana_ip_resource = panos_address_object.grafana
    hwiplab_ip_resource = panos_address_object.hwiplab
    prometheus_ip_resource = panos_address_object.prometheus
}
