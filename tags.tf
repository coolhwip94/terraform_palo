# create tag
resource "panos_administrative_tag" "internal" {
    name = "internal"
    color = "color5"
    comment = "Internal resources"
}