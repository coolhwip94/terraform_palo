# Panorama Example
resource "panos_decryption_rule_group" "main-ssl-rule-group" {

  # device_group = panos_device_group.x.name
  rulebase         = "pre-rulebase"
  position_keyword = "bottom"

  # rule for grafana
  rule {
    name        = "grafana-ssl-rule"
    description = "Made by Terraform"

    # source
    source_zones     = ["any"]
    source_addresses = ["any"]
    source_users     = ["any"]
    # source_hips      = ["any"]

    # destination
    destination_zones     = ["any"]
    destination_addresses = [panos_address_object.grafana.name]
    # destination_hips      = ["any"]
    negate_destination    = false

    services = [panos_service_object.grafana-service.name]

    url_categories = [
      "any",
    ]

    # decryption
    action          = "no-decrypt"
    decryption_type = "ssl-forward-proxy"

    log_successful_tls_handshakes = false
    log_failed_tls_handshakes     = false

  }

  # rule for prometheus
  rule {
    name        = "prometheus-ssl-rule"
    description = "Made by Terraform"

    # source
    source_zones     = ["untrusted"]
    source_addresses = ["any"]
    source_users     = ["any"]
    # source_hips      = ["any"]

    # destination
    destination_zones     = ["any"]
    destination_addresses = [panos_address_object.prometheus.name]
    # destination_hips      = ["any"]
    negate_destination    = false

    services = [panos_service_object.prometheus-service.name]

    url_categories = [
      "any",
    ]

    # decryption
    action          = "no-decrypt"
    decryption_type = "ssl-forward-proxy"

    log_successful_tls_handshakes = false
    log_failed_tls_handshakes     = false

  }
}
