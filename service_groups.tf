# create service group
resource "panos_service_group" "internal-services" {
  name = "internal-services"
  services = [
    panos_service_object.grafana-service.name,
    panos_service_object.prometheus-service.name
  ]
}
